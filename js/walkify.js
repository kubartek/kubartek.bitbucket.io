import * as THREE from '/js/three.module.js';
import { OrbitControls } from '/js/OrbitControls.js';

class Marker {
    constructor(x, y, z) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.locationId = null;
        this.pointer = null;
        this.img = null;
    }
}

class Location {
    constructor(imageUrl, markers, id) {
        this.markers = markers;
        this.imageUrl = imageUrl;
        this.is = id;
    }
}



var scene, camera, renderer, controls, group;
var container;

var imgurl, imgurl_green, imgurl_red, imgurl_yellow;
var texture, texture_green, texture_red, texture_yellow;

var sphereMaterial;
var locations = [], markers=[];

//var INTERSECTED;
//var mouse = {
//    x: 0,
//    y: 0
//};

loadResources();
initMarkers();
initLocations();
init();
animate();

//( 94.43128282141257 , -32.68686254415978 , -2.0100771841636775 ) green
//( -68.00191751116645 , -71.93523452968117 , 13.975020442818469 ) red
//( 42.57097496570994 , -23.99921631110937 , 87.19623442875498 ) yellow

function loadResources() {
    imgurl = "https://i.ibb.co/G5SckFc/103159979-928822747549381-3760670831670772442-n.jpg";
    imgurl_green = "https://i.ibb.co/XF3xXyQ/budowa-green.jpg";
    imgurl_red = "https://i.ibb.co/n1y7Wyp/budowa-red.jpg";
    imgurl_yellow = "https://i.ibb.co/26wHt5r/budowa-yellow.jpg";

    texture = new THREE.TextureLoader().load(imgurl);
}

function initLocations() {
    locations[0] = new Location(imgurl, markers, "budowa");
    locations[1] = new Location(imgurl, markers, "budowa_green");
    locations[2] = new Location(imgurl, markers, "budowa_red");
    locations[3] = new Location(imgurl, markers, "budowa_yellow");

}

function initMarkers() {
    markers[0] = new Marker(94.43, -32.69, -2.01);
    markers[0].locationId = "budowa";
    markers[0].img = imgurl_green;
    markers[1] = new Marker(-68.00, -71.94, 13.98);
    markers[1].locationId = "budowa";
    markers[1].img = imgurl_red;
    markers[2] = new Marker(42.57, -24, 87.20);
    markers[2].locationId = "budowa";
    markers[2].img = imgurl_yellow;

    group = new THREE.Group();

    var markerGeometry = new THREE.SphereGeometry(10, 10, 10);
    var m;
    for (m of markers) {
        var markerTexture = new THREE.TextureLoader().load(m.img);

        var markerMaterial = new THREE.MeshBasicMaterial(
            {
                map: markerTexture,
                opacity: 0.1,
                transparent: true,
                side: THREE.BackSide
            }
        );
        m.pointer = new THREE.Mesh(markerGeometry, markerMaterial);
        var p = getPointInBetweenByLen(new THREE.Vector3(m.x, m.y, m.z), new THREE.Vector3(0, 0, 0), 10);
        m.pointer.position.set(p.x, p.y, p.z);
        m.pointer.scale.set(-1, 1, 1);
        group.add(m.pointer);
    }
}


function init() {
    console.log("width= ", window.innerWidth);
    console.log("height= ", window.innerHeight);

    scene = new THREE.Scene();

    camera = new THREE.PerspectiveCamera(50, window.innerWidth / window.innerHeight, 1, 200);
    camera.position.set(80, 4.9, 4.9);

    renderer = new THREE.WebGLRenderer();
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(window.innerWidth, window.innerHeight);

    container = document.getElementById('Walkify');
    container.appendChild(renderer.domElement);

    controls = new OrbitControls(camera, renderer.domElement);
    controls.minDistance = 1;
    controls.maxDistance = 80;
    controls.zoomSpeed = 10;
    controls.enablePan = false;
    controls.enableKeys = false;
    controls.update();


    sphereMaterial = new THREE.MeshBasicMaterial(
        {
            map: texture,
            side: THREE.BackSide
        }
    );

    var sphere = new THREE.SphereGeometry(100, 100, 100);
    var sphereMesh = new THREE.Mesh(sphere, sphereMaterial);
    sphereMesh.scale.set(-1, 1, 1);
 
    scene.add(sphereMesh);
    scene.add(group);

    window.addEventListener('resize', onWindowResize, false);
    window.addEventListener("mousemove", onDocumentMouseMove, false);
    window.addEventListener('mousedown', onDocumentMouseDown, false);
}


function animate() {
    renderer.render(scene, camera);
    requestAnimationFrame(animate);

    //controls.update();
    //cube.rotation.x += 0.01;
    //cube.rotation.y += 0.002; 

    //console.log(camera.position);
    //line.end = camera.position;
    //lineMesh.geometry.verticesNeedUpdate = true;
    //lineMesh.points[1] = camera.posiiton;
    //if (distance(camera.position) > 100)
    //    console.log("too far");
    //console.log("distance = ", controls.scope.distance);
    //console.log("zoom", controls.scope.zoom);
    // update();
}

//function getAngle(v1, v2) {
//    v1 
//}

function distance(v1) {
    var dx = v1.x;
    var dy = v1.y;
    var dz = v1.z;

    return Math.sqrt(dx * dx + dy * dy + dz * dz);
}

function distanceVector(v1, v2) {
    var dx = v1.x - v2.x;
    var dy = v1.y - v2.y;
    var dz = v1.z - v2.z;

    return Math.sqrt(dx * dx + dy * dy + dz * dz);
}

function onWindowResize() {

    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize(window.innerWidth, window.innerHeight);

}

var selectedObject = null;

function onDocumentMouseMove(event) {

    event.preventDefault();
    if (selectedObject) {

        //selectedObject.material.color.setHex('0x00ff00');
        selectedObject.material.transparent = true;
        selectedObject = null;

    }

    var intersects = getIntersects(event.layerX, event.layerY);
    if (intersects.length > 0) {

        var res = intersects.filter(function (res) {

            return res && res.object;

        })[0];

        if (res && res.object) {

            selectedObject = res.object;
            //selectedObject.material.color.setHex('0x9b0a1e');
            selectedObject.material.transparent = false;
        }
    }
}

var raycaster = new THREE.Raycaster();
var mouseVector = new THREE.Vector3();

function getIntersects(x, y) {

    x = (x / window.innerWidth) * 2 - 1;
    y = - (y / window.innerHeight) * 2 + 1;

    mouseVector.set(x, y, 0.5);
    raycaster.setFromCamera(mouseVector, camera);

    return raycaster.intersectObject(group, true);

}

function onDocumentMouseDown(event) {
    //if (selectedObject === markers[0].pointer) {
    //    console.log("green");
    //    sphereMaterial.map = texture_green;
    //}

    if (selectedObject) {
        sphereMaterial.map = selectedObject.material.map;
    }
}

function getPointInBetweenByLen(start, end, length) {

    var dir = end.clone().sub(start).normalize().multiplyScalar(length);
    return start.clone().add(dir);

}